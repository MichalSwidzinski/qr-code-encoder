from django import views
from django.contrib import admin
from django.urls import include, path
from qr_code_encoder import views

urlpatterns = [
    path('', include('qr_code_encoder.urls')),
    path('admin/', admin.site.urls),    
]