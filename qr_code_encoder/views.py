import segno

from django.shortcuts import render


def index(request):
    if request.method == 'POST':
        qr_string = request.POST["input_qr_string"]
        qr_code = segno.make(qr_string)
        ctx = {"qr_code": qr_code.svg_data_uri(dark='darkblue', scale=8)}

        return render(request, 'view_qr_code.html', ctx)

    return render(request, "index.html")
